


import { Login } from "../actionTypes";
// import { login  } from


const initialState = {
    customer_details: {},
    name: 'First Ladies'
};

function rootReducer(state = initialState, action: any) {
    if (action.type === Login) {
        state.customer_details = action.payload;
    }
    return state;
}

export default rootReducer;