import React, {Component} from 'react'
import './App.css';
import {Container, Grid} from '@material-ui/core';
import {BrowserRouter as Router, Redirect, Route, Switch} from 'react-router-dom';
import Home from './components/home'
import Navigation from './components/navigation'
import Footer from './components/footer'
import Productpage from './components/productpage'
import productdetailpage from './components/productdetailpage'
import Location from './components/location'
import Subscribe from './components/subscribe'
import contactUs from "./components/contactUs";
import registration from './components/registration'
import login from './components/login'
import 'bootstrap/dist/css/bootstrap.min.css';
import profile from "./components/profile";



class App extends Component {
    render() {
        return (
            <Router>
                <Grid className="App"
                      style={{backgroundColor: '#f0f0f0', minHeight: 'calc(100vh)', paddingBottom: '50px'}}>
                    {/*
					//@ts-ignore */}
                    <Navigation {...this.props} />
                    <Container fixed>
                        <Switch>
                            <Redirect exact from="/" to="/home"/>
                            {/*
							//@ts-ignore */}
                            <Route exact path="/home" component={Home}/>
                            <Route exact path="/productpage" component={Productpage}/>
                            <Route exact path="/productpage/:category" component={Productpage}/>
                            <Route exact path="/productdetailpage/:id" component={productdetailpage}/>
                            <Route exact path="/subs/:email" component={Subscribe}/>
                            <Route exact path="/location" component={Location}/>
                            <Route exact path="/contactus" component={contactUs}/>
                            <Route exact path="/registration" component={registration}/>
                            <Route exact path="/login" component={login}/>
                            <Route exact path="/profile" component={profile}/>
                            <Route exact path="/profile/:link" component={profile}/>
                            <Redirect exact from="/*" to="/home"/>
                        </Switch>
                    </Container>
                </Grid>
                <Footer/>
            </Router>
        )
    }
}


export default App;