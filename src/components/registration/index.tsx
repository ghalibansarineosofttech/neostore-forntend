import React, {Component} from 'react'
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {KeyboardDatePicker, MuiPickersUtilsProvider} from '@material-ui/pickers';
// import {toastError, toastSuccess} from '../utils/toast'
import {InputAdornment, Paper, TextField} from '@material-ui/core';
// import {Paper, TextField} from '@material-ui/core';
import Button from '@material-ui/core/Button';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import IconButton from '@material-ui/core/IconButton';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import {withOutTokenPost} from "../../helper/AxiosGlobal";
import {RemoveRedEye} from '@material-ui/icons';
// import {Loader} from '../utils/loader'
import {Constants} from '../../helper/constant'
import Grid from "@material-ui/core/Grid";
import {toastError, toastSuccess} from "../utils/toast";
import {Loader} from "../utils/loader";
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import VisibilityIcon from '@material-ui/icons/Visibility';



//style
const customStyle = {
    mainDiv: {
        display: 'flex',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    div: {
        // height: 750,
        width: 450,
        marginTop: 50,
        paddingTop: 10,
        paddingBottom: 50,
        marginBottom: 50,
        borderColor: 'black',
    },
    input: {width: '70%'},
    loading: false,
};

class registration extends Component {
    state = {
        firstName: '',
        lastName: '',
        email: '',
        password: '',
        cpassword: '',
        phone: '',
        gender: 'Male',
        errors: {
            firstNameErr: '',
            lastNameErr: '',
            emailErr: '',
            passwordErr: '',
            cpasswordErr: '',
            phoneErr: '',
            genderErr: '',
        },
        passwordIsMasked: true,
        cpasswordIsMasked: true,
        loading: false,
    };

    componentDidMount() {
        const token: any = localStorage.getItem('token');
        if (token !== null) {
            //@ts-ignore
            this.props.history.push('/')
        }
    }

    togglePasswordMask = () => { this.setState({ passwordIsMasked: !this.state.passwordIsMasked }) };
    ctogglePasswordMask = () => { this.setState({ cpasswordIsMasked: !this.state.cpasswordIsMasked }) };

    /**
     * Onchange event listner for all input's
     * @param {event,   type: which button.}
     */
    inputChange = async (event: any, inputType: any) => {
        await this.setState({[inputType]: event.target.value});
        const {firstName, lastName, email, password, cpassword, phone, gender} = this.state;
        const firstNameReg: any = Constants.nameReg
        const lastNameReg: any = Constants.nameReg
        const passReg: any = Constants.passwordReg
        // eslint-disable-next-line no-useless-escape
        const emailReg: any = Constants.emailReg
        const phoneReg: any = Constants.phoneReg
        await this.setState((prevState: any) => {
            let errors = {...prevState.errors};
            if (inputType === 'firstName') {
                if (firstName === '' ) { errors.firstNameErr = Constants.required }
                else if (!firstNameReg.test(firstName)) { errors.firstNameErr = Constants.nameErr }
                else { errors.firstNameErr = Constants.blank }
            }
            if (inputType === 'lastName') {
                if (lastName === '' ) { errors.lastNameErr = Constants.required }
                else if (!lastNameReg.test(lastName)) { errors.lastNameErr = Constants.nameErr }
                else { errors.lastNameErr = Constants.blank }
            }
            if (inputType === 'email') {
                if (email === '' ) { errors.emailErr = Constants.required }
                else if (!emailReg.test(email)) { errors.emailErr = Constants.emailErr }
                else { errors.emailErr = Constants.blank }
            }
            if (inputType === 'password') {
                if (password === '' ) { errors.passwordErr = Constants.required }
                else if (!passReg.test(password)) { errors.passwordErr = Constants.passwordErr }
                else if (password !== cpassword && cpassword !== '') { errors.passwordErr = 'Password does not match' }
                else { errors.passwordErr = Constants.blank }
            }
            if (inputType === 'cpassword') {
                if (cpassword === '' ) { errors.cpasswordErr = Constants.required }
                else if (!passReg.test(cpassword)) { errors.cpasswordErr = Constants.passwordErr }
                else if (password!==cpassword) { errors.cpasswordErr = 'Password does not match' }
                else { errors.cpasswordErr = Constants.blank }
            }
            if (inputType === 'phone') {
                if (phone === '' ) { errors.phoneErr = Constants.required }
                else if (!phoneReg.test(phone)) { errors.phoneErr = Constants.phoneErr }
                else { errors.phoneErr = Constants.blank }
            }
            return {errors}
        })
    };

    //Onchange file input.
    fileChange = (event: any, type: any) => {
        this.setState({[type]: event.target.files[0]})
    };

    //reset api.
    registerapi = async () => {
        this.setState({loading: true});
        const {firstName, lastName, email, password, cpassword, phone, gender} = this.state;
        const firstNameReg: any = Constants.nameReg
        const lastNameReg: any = Constants.nameReg
        const passReg: any = Constants.passwordReg
        // eslint-disable-next-line no-useless-escape
        const emailReg: any = Constants.emailReg
        const phoneReg: any = Constants.phoneReg

        await this.setState((prevState: any) => {
            let errors = {...prevState.errors};
            if (firstName === '' ) { errors.firstNameErr = Constants.required }
            else if (!firstNameReg.test(firstName)) { errors.firstNameErr = Constants.nameErr }
            else { errors.firstNameErr = Constants.blank }
        
            if (lastName === '' ) { errors.lastNameErr = Constants.required }
            else if (!lastNameReg.test(lastName)) { errors.lastNameErr = Constants.nameErr }
            else { errors.lastNameErr = Constants.blank }
        
            if (email === '' ) { errors.emailErr = Constants.required }
            else if (!emailReg.test(email)) { errors.emailErr = Constants.emailErr }
            else { errors.emailErr = Constants.blank }
        
            if (password === '' ) { errors.passwordErr = Constants.required }
            else if (!passReg.test(password)) { errors.passwordErr = Constants.passwordErr }
            else { errors.passwordErr = Constants.blank }
        
            if (cpassword === '' ) { errors.cpasswordErr = Constants.required }
            else if (password!==cpassword) { errors.cpasswordErr = 'Password does not match' }
            else { errors.cpasswordErr = Constants.blank }
        
            if (phone === '' ) { errors.phoneErr = Constants.required }
            else if (!phoneReg.test(phone)) { errors.phoneErr = Constants.phoneErr }
            else { errors.phoneErr = Constants.blank }
            
            return {errors}
        });

        if (firstNameReg.test(firstName) && lastNameReg.test(lastName) && emailReg.test(email) && passReg.test(password) && password===cpassword && phoneReg.test(phone)) {
            const data = {first_name: firstName, last_name: lastName, email, pass: password, confirmPass: cpassword, phone_no: phone, gender}
            try {
                let res = await withOutTokenPost('register', data);
                if (res.data.success) {
                    toastSuccess(res.data.message);
                    this.setState({loading: false});
                    //@ts-ignore
                    this.props.history.push('/login')
                } else {
                    this.setState({loading: false});
                    toastError("Something went wrong.")
                }
            } catch (err) {
                this.setState({loading: false});
                toastError(`${err.response.data.message || err.response.data.error_message}. please try again.`)
            }
        }
        this.setState({loading: false})
    };

    handleDateChange = (date: Date, prevState: any) => {
        this.setState({dob: date})
    };

    render() {
        const {passwordIsMasked, cpasswordIsMasked} = this.state
        return (
            <div style={customStyle.mainDiv}>
                {/*
                //@ts-ignore */}
                 <Loader loading={this.state.loading}/>
                <Paper elevation={4} style={customStyle.div} variant='elevation'>
                    <h1>Social Login.</h1>
                    <Grid xs={12} style={{display: 'flex'}}>
                        <Grid xs={6}><Button size='large' color='secondary' variant="contained">Google +</Button></Grid>
                        <Grid xs={6}><Button size='large' color='primary' variant="contained">Facebook</Button></Grid>
                    </Grid>
                    <br/>
                    <hr />
                    <h1>Registration.</h1>
                    <TextField
                        required
                        id=""
                        helperText={this.state.errors.firstNameErr}
                        //@ts-ignore
                        error={!!this.state.errors.firstNameErr}
                        label="First Name"
                        type="firstName"
                        autoComplete="firstName"
                        style={customStyle.input}
                        onChange={event => this.inputChange(event, 'firstName')}
                    />
                    <br/><br/>
                    <TextField
                        required
                        id=""
                        label="Last Name"
                        type="lastName"
                        helperText={this.state.errors.lastNameErr}
                        //@ts-ignore
                        error={!!this.state.errors.lastNameErr}
                        autoComplete=""
                        style={customStyle.input}
                        onChange={event => this.inputChange(event, 'lastName')}
                    />
                    <br/><br/>
                    <TextField
                        required
                        id="Email"
                        label="Email"
                        type="email"
                        helperText={this.state.errors.emailErr}
                        //@ts-ignore
                        error={!!this.state.errors.emailErr}
                        // autoComplete=""
                        style={customStyle.input}
                        onChange={event => this.inputChange(event, 'email')}
                    />
                    <br/><br/>
                    <TextField
                        required
                        id="Password"
                        label="Password"
                        type={passwordIsMasked ? 'password' : 'text'}
                        helperText={this.state.errors.passwordErr}
                        //@ts-ignore
                        error={!!this.state.errors.passwordErr}
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    <IconButton
                                        aria-label="toggle password visibility"
                                        onClick={this.togglePasswordMask}
                                    >
                                        {/*<RemoveRedEye/>*/}
                                        {passwordIsMasked ? <VisibilityOffIcon/> : <VisibilityIcon/>}
                                    </IconButton>
                                </InputAdornment>
                            ),
                        }}
                        // autoComplete=""
                        style={customStyle.input}
                        onChange={event => this.inputChange(event, 'password')}
                    />
                    <br/><br/>
                    <TextField
                        required
                        id="CPassword"
                        label="Confirm Password"
                        type={cpasswordIsMasked ? 'password' : 'text'}
                        helperText={this.state.errors.cpasswordErr}
                        //@ts-ignore
                        error={!!this.state.errors.cpasswordErr}
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    <IconButton
                                        aria-label="toggle password visibility"
                                        onClick={this.ctogglePasswordMask}
                                    >
                                        {/*<RemoveRedEye/>*/}
                                        {/*
                                        //@ts-ignore */}
                                       {cpasswordIsMasked ? <VisibilityOffIcon/> : <VisibilityIcon/>}
                                    </IconButton>
                                </InputAdornment>
                            ),
                        }}
                        // autoComplete=""
                        style={customStyle.input}
                        onChange={event => this.inputChange(event, 'cpassword')}
                    />
                    <br/><br/>
                    <TextField
                        required
                        id="Phone"
                        label="Phone"
                        type="Number"
                        helperText={this.state.errors.phoneErr}
                        //@ts-ignore
                        error={!!this.state.errors.phoneErr}
                        // autoComplete=""
                        style={customStyle.input}
                        onChange={event => this.inputChange(event, 'phone')}
                    />
                    <br/><br/><br/>
                    <div style={{width: '70%', marginLeft: 'auto', marginRight: 'auto', textAlign: 'left'}}>
                        <FormLabel component="legend">Gender *</FormLabel>
                        <RadioGroup row style={{marginLeft: 'auto', marginRight: 'auto', alignItems: 'left'}}
                                    aria-label="gender" name="gender1" value={this.state.gender}
                                    onChange={event => this.inputChange(event, 'gender')}>
                            <FormControlLabel value="Male" control={<Radio/>} label="Male"/>
                            <FormControlLabel value="Female" control={<Radio/>} label="Female"/>
                            {/* <FormControlLabel value="2" control={<Radio/>} label="Other"/> */}
                        </RadioGroup>
                    </div>
                    <br/><br/>
                    <div style={{display: 'flex', justifyContent: 'center'}}>
                        <div style={{display: 'flex', width: '70%', justifyContent: 'space-around'}}>
                            <Button onClick={this.registerapi} variant="contained" color="primary">Registration</Button>
                        </div>
                    </div>
                </Paper>
            </div>
        )
    }
}


export default registration