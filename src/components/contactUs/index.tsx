import React, {Component} from 'react'
import {Grid, Paper, TextField} from "@material-ui/core"
import Button from '@material-ui/core/Button'
import {Constants} from "../../helper/constant"
import {withOutTokenPost} from '../../helper/AxiosGlobal'


const customStyle = {
    mainDiv: {
        display: 'flex',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    div: {
        // height: 350,
        width: 450,
        marginTop: 100,
        marginBottom: 50,
        paddingTop: 20,
        paddingBottom: 50,
        borderColor: 'black',
    },
    input: {width: '70%', height: '50px', paddingBottom: '15px'}
};


class contactUs extends Component {
    state = {
        name: '',
        email: '',
        phone: '',
        subject: '',
        message: '',
        errors: {
            nameErr: '',
            emailErr: '',
            phoneErr: '',
            subjectErr: '',
            messageErr: '',
        },
        loading: false,
    };

    inputChange = async (event: any, inputType: any) => {
        await this.setState({[inputType]: event.target.value});
        const {name, email, message, phone, subject} = this.state;
        const nameReg: any = Constants.nameReg;
        const mailReg: any = Constants.emailReg;
        const phoneReg: any = Constants.phoneReg;
        await this.setState((prevState: any) => {
            let errors = {...prevState.errors};
            if (inputType === 'name') {
                //@ts-ignore
                if (name === '') {
                    errors.nameErr = Constants.required
                } else if (!nameReg.test(name)) {
                    errors.nameErr = Constants.nameErr
                } else {
                    errors.nameErr = ''
                }
            }
            if (inputType === 'email') {
                //@ts-ignore
                if (email === '') {
                    errors.emailErr = Constants.required
                } else if (!mailReg.test(email)) {
                    errors.emailErr = Constants.emailErr
                } else {
                    errors.emailErr = ''
                }
            }
            if (inputType === 'phone') {
                //@ts-ignore
                if (phone === '') {
                    errors.phoneErr = Constants.required
                } else if (!phoneReg.test(phone)) {
                    errors.phoneErr = Constants.phoneErr
                } else {
                    errors.phoneErr = ''
                }
            }
            if (inputType === 'subject') {
                //@ts-ignore
                if (subject === '') {
                    errors.subjectErr = Constants.required
                } else if (!nameReg.test(subject)) {
                    errors.subjectErr = Constants.nameErr
                } else {
                    errors.subjectErr = ''
                }
            }
            if (inputType === 'message') {
                //@ts-ignore
                if (message === '') {
                    errors.messageErr = Constants.required
                } else if (!nameReg.test(message)) {
                    errors.messageErr = Constants.nameErr
                } else {
                    errors.messageErr = ''
                }
            }
            return {errors}
        })
    };

    submit = async () => {
        const {name, email, message, phone, subject} = this.state;
        const nameReg: any = Constants.nameReg;
        const mailReg: any = Constants.emailReg;
        const phoneReg: any = Constants.phoneReg;
        await this.setState((prevState: any) => {
            let errors = {...prevState.errors};
            if (name === '') {
                errors.nameErr = Constants.required
            } else if (!nameReg.test(name)) {
                errors.nameErr = Constants.nameErr
            } else {
                errors.nameErr = ''
            }

            if (email === '') {
                errors.emailErr = Constants.required
            } else if (!mailReg.test(email)) {
                errors.emailErr = Constants.emailErr
            } else {
                errors.emailErr = ''
            }

            if (phone === '') {
                errors.phoneErr = Constants.required
            } else if (!phoneReg.test(phone)) {
                errors.phoneErr = Constants.phoneErr
            } else {
                errors.phoneErr = ''
            }

            if (subject === '') {
                errors.subjectErr = Constants.required
            } else if (!nameReg.test(subject)) {
                errors.subjectErr = Constants.nameErr
            } else {
                errors.subjectErr = ''
            }

            if (message === '') {
                errors.messageErr = Constants.required
            } else if (!nameReg.test(message)) {
                errors.messageErr = Constants.nameErr
            } else {
                errors.messageErr = ''
            }

            return {errors}
        });

        if (nameReg.test(name) && mailReg.test(email) && phoneReg.test(phone) && nameReg.test(subject) && nameReg.test(message)) {
            try {
                let res = await withOutTokenPost('/contactUs', {
                    customer_id: 0,
                    name,
                    email,
                    message,
                    phone_no: phone,
                    subject
                });
                await console.log("res", res)
                // if (res.data.success) {
                // this.setState({loading: false});
                // if (res.data.data.user.first_login) {
                // toastWarning("Please Change your password.");
                // } else {
                // toastSuccess("Login Successfully.");
                // }
                // }
            } catch (err) {
                this.setState({loading: false});
                // toastError(`${err.response?.data?.message}.`)
            }
        }
    };

    render() {
        return (
            <Grid container xs={12} style={{display: 'flex', justifyContent: 'center'}}>
                <Grid item xs={12} sm={10} md={8} style={customStyle.mainDiv}>
                    <Paper style={customStyle.div} variant='elevation'>
                        <h1>Contact Us.</h1>
                        <TextField
                            required
                            //@ts-ignore
                            error={!!this.state.errors.nameErr}
                            helperText={this.state.errors.nameErr}
                            label="Name"
                            type="name"
                            style={customStyle.input}
                            onChange={event => this.inputChange(event, 'name')}
                        />
                        <TextField
                            required
                            //@ts-ignore
                            error={this.state.errors.emailErr ? true : false}
                            helperText={this.state.errors.emailErr}
                            label="Email"
                            type="email"
                            // autoComplete="current-password"
                            style={customStyle.input}
                            onChange={event => this.inputChange(event, 'email')}
                        />
                        <TextField
                            required
                            //@ts-ignore
                            error={!!this.state.errors.emailErr}
                            helperText={this.state.errors.emailErr}
                            label="Phone Number"
                            type="number"
                            // autoComplete="current-password"
                            style={customStyle.input}
                            onChange={event => this.inputChange(event, 'phone')}
                        />
                        <TextField
                            required
                            //@ts-ignore
                            error={!!this.state.errors.subjectErr}
                            helperText={this.state.errors.subjectErr}
                            label="Subject"
                            type="subject"
                            // autoComplete="current-password"
                            style={customStyle.input}
                            onChange={event => this.inputChange(event, 'subject')}
                        />
                        <TextField
                            required
                            //@ts-ignore
                            error={!!this.state.errors.messageErr}
                            helperText={this.state.errors.messageErr}
                            label="Message"
                            type="message"
                            // autoComplete="current-password"
                            style={customStyle.input}
                            onChange={event => this.inputChange(event, 'message')}
                        />
                        <div style={{display: 'flex', justifyContent: 'center', paddingTop: '30px'}}>
                            <div style={{display: 'flex', width: '70%', justifyContent: 'space-around'}}>
                                <Button onClick={() => {
                                    this.submit()
                                }} variant="contained" color="primary">Submit</Button>
                            </div>
                        </div>
                    </Paper>
                </Grid>
            </Grid>
        );
    }
}

export default contactUs;