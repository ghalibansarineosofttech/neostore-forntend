import React, {Component} from 'react'
import Grid from '@material-ui/core/Grid';
import {withOutTokenGet} from '../../helper/AxiosGlobal'
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {Constants} from '../../helper/constant';
import Rating from '@material-ui/lab/Rating';
import Box from '@material-ui/core/Box';
//@ts-ignore
import Carousel from 'react-material-ui-carousel'

export default class Home extends Component {
    state = {
        cards: [],
        slides: []
    };

    componentDidMount() {
        withOutTokenGet('defaultTopRatingProduct')
            .then((data) => {
                this.setState({cards: data.data.product_details})
            });
        withOutTokenGet('getAllCategories')
            .then((data) => {
                this.setState({slides: data.data.category_details})
            })
            .then((x) => console.log(this.state.slides))

    }

    nocard = () => {
        return (<h1>No Products Available.</h1>)
    };

    card = () => {
        let {cards} = this.state;
        return cards.map((value: any) => {
            return (
                <Grid item key={value.DashboardProducts[0]._id} xs={12} sm={6} md={3} xl={2}>
                    <Box boxShadow={3}>
                        <Card style={{paddingBottom: '10px'}}>
                            {/*
                    //@ts-ignore */}
                            <CardActionArea onClick={() => {
                                {/*
                                //@ts-ignore */}
                                this.props.history.push(`/productdetailpage/${value.DashboardProducts[0].product_id}`);
                                // console.log("/productdetailpage/${value.DashboardProducts[0].product_id}",value.DashboardProducts[0].product_id);
                            }}>
                                <CardMedia
                                    component="img"
                                    style={{height: '200px'}}
                                    image={`${Constants.baseURL}${value.DashboardProducts[0].product_image}`}
                                    title={value.DashboardProducts[0].product_name}
                                />
                                <CardContent>
                                    <Typography gutterBottom>{value.DashboardProducts[0].product_name}</Typography>
                                </CardContent>
                            </CardActionArea>
                            <CardActions>
                                <Grid container item style={{display: 'flex', justifyContent: 'center'}}>
                                    <Grid xs={12} sm={4} md={4}
                                          xl={4}>Rs.{value.DashboardProducts[0].product_cost}</Grid>
                                    <Grid xs={12} sm={8} md={8} xl={8}><Rating name="read-only"
                                                                               value={value.DashboardProducts[0].product_rating}
                                                                               readOnly/></Grid>
                                    <br/><Grid xs={10} style={{marginTop: '10px'}}><Button variant="contained"
                                                                                           color="primary">Add to
                                    Cart</Button></Grid><br/>
                                </Grid>
                            </CardActions>
                        </Card>
                    </Box>
                </Grid>
            )
        })
    };

    render() {
        const {cards} = this.state;
        return (
            <Grid container
                  style={{display: 'flex', justifyContent: 'center', paddingTop: '50px', paddingBottom: '170px'}}>
                <Grid item xs={12} sm={12} md={12} xl={12}>
                    <Carousel animation="slide" interval="2000" indicator={true}>
                        {
                            this.state.slides.map(item => {
                                return (
                                    <div>
                                        {/*
                                //@ts-ignore */}
                                        <img onClick={() => {
                                            {/*
                                            //@ts-ignore */}
                                            this.props.history.push(`/productpage/${item.category_id}`)
                                            {/*
                                            //@ts-ignore */}
                                        }} src={`${Constants.baseURL}${item.product_image}`}
                                             style={{width: "100%", height: '500px'}}/>
                                    </div>
                                )
                            })
                        }
                    </Carousel>
                </Grid>

                <Grid container item style={{display: 'flex', flexDirection: 'column', paddingBottom: '10px'}} xs={12}
                      sm={12} md={12} xl={10}>
                    <Grid item><h1>Popular Products</h1></Grid>
                    {/*
                    //@ts-ignore */}
                    <Grid item><Button color="primary" onClick={() => {
                        {/*
                        //@ts-ignore */}
                        this.props.history.push('/productpage')
                    }}>View All</Button></Grid>
                </Grid>

                <Grid container item xs={12} sm={12} md={12} xl={10} spacing={3}>
                    {/*
                    //@ts-ignore */}
                    {cards.length === 0 ? this.nocard() : this.card()}
                </Grid>
            </Grid>
        )
    }
}
