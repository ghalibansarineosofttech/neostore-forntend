import React, {Component} from 'react';
import {Link, withRouter} from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import {Navbar, Nav, NavDropdown, Form, FormControl, Button as BButton} from 'react-bootstrap'
import IconButton from '@material-ui/core/IconButton';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
// import ButtonComponent from './ButtonComponent'
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import {GridList} from "@material-ui/core";
// @import "~bootstrap/scss/bootstrap";
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import Badge from '@material-ui/core/Badge';





//Style's.
const customStyles = {
  header: {
      display: 'flex',
      justifyContent: 'space-between'
  },
  maindiv: {
    //   width: '100%',
      display: 'flex',
      justifyContent: 'space-between'
  },
  subdiv: {display: 'flex'}
};




//Navigation.
class Navigation extends Component {
    state = {
        anchorEl: null
    }

    handleClick = (event: any) => {
        this.setState({anchorEl: event.currentTarget});
    };

    handleClose = () => {
        this.setState({anchorEl: null});
    };

    logout = () => {
        localStorage.clear();
        //@ts-ignore
        this.props.history.push('/login')
    }

    render() {
        const {anchorEl} = this.state;
        return (
            <Navbar style={{background: '#000099'}} expand="md">
                <Navbar.Brand href="/" style={{color: "white", display: "flex"}}>
                    <Typography variant="h4" style={{color: 'white'}}>Neo</Typography>
                    <Typography variant="h4" style={{color: 'yellow'}}>Store</Typography>
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav"
                                 // style={{display: 'flex'}}
                >
                    <Nav className="mr-auto">
                        <Nav.Link ><Link to='/' style={{color: "white", textDecoration: 'none'}}>Home</Link></Nav.Link>
                        <Nav.Link ><Link to='/productpage' style={{color: "white", textDecoration: 'none'}}>Products</Link></Nav.Link>
                        <Nav.Link ><Link to='/profile/Order' style={{color: "white", textDecoration: 'none'}}>Order</Link></Nav.Link>
                    </Nav>
                    <Form>
                        <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                    </Form>
                    <Button style={{color: "white", marginLeft: 15}}>
                    <Badge badgeContent={4} color="error"><AddShoppingCartIcon fontSize='large' /></Badge>
                    </Button>
                    <NavDropdown title="Profile" id="basic-nav-dropdown" style={{color: "white"}}>
                        <NavDropdown.Item ><Link to='/login' style={{color: "black"}} >Login</Link></NavDropdown.Item>
                        <NavDropdown.Item ><Link to='/registration' style={{color: "black"}}>Register</Link></NavDropdown.Item>
                        <NavDropdown.Item ><Link to='/profile' style={{color: "black"}}>Profile</Link></NavDropdown.Item>
                        {/*
                        //@ts-ignore */}
                        <NavDropdown.Item ><Link onClick={()=>{this.logout()}} style={{color: "black"}}>Logout</Link></NavDropdown.Item>
                    </NavDropdown>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}


//@ts-ignore
export default (withRouter(Navigation))