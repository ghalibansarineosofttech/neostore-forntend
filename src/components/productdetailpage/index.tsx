import React, {Component} from 'react'
import Grid from '@material-ui/core/Grid';
import {withOutTokenGet} from '../../helper/AxiosGlobal'
// import { makeStyles, Theme } from '@material-ui/core/styles';
// import AppBar from '@material-ui/core/AppBar';
// import Tabs from '@material-ui/core/Tabs';
// import Tab from '@material-ui/core/Tab';
// import Box from '@material-ui/core/Box';
// import React from 'react';
// import Typography from '@material-ui/core/Typography';
// import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
// import Typography from '@material-ui/core/Typography';
// import Rating from '@material-ui/lab/Rating';
import {Constants} from '../../helper/constant';
import Rating from '@material-ui/lab/Rating';
//@ts-ignore
import ImageGallery from 'react-image-gallery';
import "react-image-gallery/styles/css/image-gallery.css";
//@ts-ignore
import {Tab, TabList, TabPanel, Tabs} from 'react-tabs';
import 'react-tabs/style/react-tabs.css';


export default class productdetailpage extends Component {
    state = {
        product_details: []
    }

    componentDidMount(){
        //@ts-ignore
        if(this.props.match.params.id){
            //@ts-ignore
            withOutTokenGet(`getProductByProdId/${this.props.match.params.id}`)
            .then((data) => { this.setState({product_details: data.data.product_details}) })
        }
        else {
            //@ts-ignore
            this.props.history.push('/')
        }
    }


    productdetail = () => {
        const {product_details} = this.state
        let imagesItems: any = []
        //@ts-ignore
        product_details[0]?.subImages_id?.product_subImages.forEach((val:any)=>{
            imagesItems.push({ original: `${Constants.baseURL}${val}`, thumbnail: `${Constants.baseURL}${val}`})
        })
        return(
            <Grid container item xs={12} style={{display: 'flex', justifyContent: 'center'}}>
                <Grid container item xs={12} md={6}>
                    <ImageGallery items={imagesItems} infinite={true} lazyLoad={true} autoPlay={true} slideInterval={3000} />
                </Grid>
                <Grid container item xs={12} md={6}>
                    {/*
                    //@ts-ignore */}
                    {console.log("product_details",product_details[0])}
                    <Grid item xs={12}>
                    {/*
                    //@ts-ignore */}
                    <p item xs={12} style={{fontSize: '40px', paddingLeft: '50px'}}>{product_details[0]?.product_name}</p>
                    </Grid>
                    <Grid item xs={12}>
                    {/*
                    //@ts-ignore */}
                    <Rating name="size-large" size="large" defaultValue={product_details[0]?.product_rating} readOnly />
                    </Grid>
                    <Grid item xs={12}>
                        {/*
                        //@ts-ignore */}
                        Price: ₹{product_details[0].product_cost}
                    </Grid>
                    <Grid item xs={12}>
                        {/*
                        //@ts-ignore */}
                        Color: <Button size="small" variant="contained" style={{backgroundColor: `${product_details[0]?.color_id?.color_code}`, height: '35px'}} />
                    </Grid>
                    <Grid item xs={12}>
                        {/*
                        //@ts-ignore */}
                        <Button variant="contained" color='primary'>ADD TO CART</Button>
                        <Button variant="contained" color='secondary'>RATE PRODUCT</Button>
                    </Grid>
                </Grid>
                <Grid container item xs={12}>
                    <Tabs style={{width: '100%', height: '170px'}}>
                        <TabList>
                            <Tab>Description</Tab>
                            <Tab>Features</Tab>
                        </TabList>
                        <TabPanel>
                            {/*
                            //@ts-ignore */}
                            <p>{product_details[0]?.product_desc}</p>
                        </TabPanel>
                        <TabPanel>
                            <Grid container xs={12} style={{display: 'flex'}}>
                                <Grid item xs={12} style={{display: 'flex'}}>                                
                                {/*
                                //@ts-ignore */}
                                <p>Dimensions: {product_details[0]?.product_dimension}</p>
                                </Grid>
                                <Grid item xs={12} style={{display: 'flex'}}>
                                {/*
                                //@ts-ignore */}
                                <p>Material: {product_details[0]?.product_material}</p>
                                </Grid>
                                <Grid item xs={12} style={{display: 'flex'}}>
                                {/*
                                //@ts-ignore */}
                                <p>Manufacturer: {product_details[0]?.product_producer}</p>
                                </Grid>
                            </Grid>
                        </TabPanel>
                    </Tabs>
                </Grid>
            </Grid>
        )
    }

    render() {
        const {product_details} = this.state
        return (
            <Grid container style={{display: 'flex', justifyContent: 'center', paddingTop: '50px'}}>
                {product_details.length===0 ? <h1>Wrong Product</h1> : this.productdetail() }
            </Grid>
        )
    }
}
