import React, { Component } from 'react'
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';

export default class Footer extends Component {
    state = {
        email: ''
    }


    render() {
        return (
            <Grid container xs={12} spacing={3} style={{paddingTop: '20px', display: 'flex', background: '#000099', fontSize: '15px', textAlign: 'center'}}>
                <Grid item xs={12} md={4}>
                    <div style={{color: 'white'}}>
                        <h3>About Company</h3>
                        <p>NeoSOFT Technologies is here at your quick and easy service for shoping.<br />
                        Contact information.<br />
                        Email: contact@neosofttech.com<br />
                        Phone: +91 0000000000<br />
                        MUMBAI, INDIA</p>
                        </div>
                </Grid>
                <Grid item xs={12} md={4}>
                    <div style={{color: 'white'}}>
                        <h3>Information</h3>
                        <Link style={{textDecoration: 'none', color: 'white'}} target="_blank" href="http://180.149.241.208:3022/2019-06-28T06-10-29.263ZTerms_and_Conditions.pdf" >Terms and Conditions</Link><br />
                        <Link style={{textDecoration: 'none', color: 'white'}} target="_blank" href="http://180.149.241.208:3022/2019-06-28T06-11-38.277ZGuarantee_ReturnPolicy.pdf" >Gurantee and Return Policy</Link><br />
                        <Link style={{textDecoration: 'none', color: 'white'}} href="/contactus" >Contact Us</Link><br />
                        <Link style={{textDecoration: 'none', color: 'white'}} target="_blank" href="#" >Privacy Policy</Link><br />
                        <Link style={{textDecoration: 'none', color: 'white'}} target="_blank" href="/location" >Locate Us</Link>
                    </div>
                </Grid>
                <Grid item xs={12} md={4}>
                    <div style={{color: 'white'}}>
                        <h3>Newsletter.</h3>
                        <p>Signup to get exclusive offer from our favorite brands and to be well up in the news.<br /><br />
                        <input onChange={(event)=>this.setState({email: event.target.value})}></input><br /><br />
                        {/*
                        //@ts-ignore */}
                        <button><Link style={{textDecoration: 'none'}} href={`/subs/${this.state.email}`} >Subscribe.</Link></button></p>
                    </div>
                </Grid>
                <p style={{width:"100%", color: 'white'}}>Copyright 2020 NeoSOFT Technologies All rights reserved | Design By Ghalib Ansari</p>
            </Grid>
        )
    }
}
