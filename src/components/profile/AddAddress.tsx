import React,{Component} from 'react'
import {Button, Grid, IconButton, InputAdornment, Paper, TextField} from "@material-ui/core";
import {RemoveRedEye} from "@material-ui/icons";
import {withOutTokenPost, withTokenDelete, withTokenGet, withTokenPost} from "../../helper/AxiosGlobal";
import {toastError, toastSuccess, toastWarning} from "../utils/toast";
import {Constants} from "../../helper/constant";
import {Link} from 'react-router-dom'


//style
const customStyle = {
    mainDiv: {
        display: 'flex',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    div: {
        // height: 750,
        width: 450,
        marginTop: 20,
        paddingTop: 10,
        paddingBottom: 20,
        marginBottom: 20,
        borderColor: 'black',
        textAlign: 'center'
    },
    input: {width: '70%'}
};

class AddAddress extends Component{
    state = {
        address_id: '',
        address: '',
        city: '',
        pincode: '',
        state: '',
        country: '',
        errors: {
            addressErr: '',
            cityErr: '',
            pincodeErr: '',
            stateErr: '',
            countryErr: '',
        }
    }

    inputChange = async (event: any, inputType: any) => {
        await this.setState({[inputType]: event.target.value});
        const {address, city, pincode, state, country} = this.state;
        await this.setState((prevState: any) => {
            let errors = {...prevState.errors};
            if (inputType === 'address') {
                if (address === '' ) { errors.addressErr = Constants.required }
                // else if (!mailReg.test(email)) { errors.emailErr = Constants.emailErr }
                else { errors.addressErr = '' }
            }
            if (inputType === 'city') {
                if (city === '' ) { errors.cityErr = Constants.required }
                // else if (!passwordReg.test(password)) { errors.passwordErr = Constants.passwordErr }
                else { errors.cityErr = '' }
            }
            if (inputType === 'pincode') {
                if (pincode === '' ) { errors.pincodeErr = Constants.required }
                // else if (!passwordReg.test(password)) { errors.passwordErr = Constants.passwordErr }
                else { errors.pincodeErr = '' }
            }
            if (inputType === 'state') {
                if (state === '' ) { errors.stateErr = Constants.required }
                // else if (!passwordReg.test(password)) { errors.passwordErr = Constants.passwordErr }
                else { errors.stateErr = '' }
            }
            if (inputType === 'country') {
                if (country === '' ) { errors.countryErr = Constants.required }
                // else if (!passwordReg.test(password)) { errors.passwordErr = Constants.passwordErr }
                else { errors.countryErr = '' }
            }
            return {errors}
        })
    }

    submit = async () => {
        const {address, city, pincode, state, country} = this.state;
        await this.setState((prevState: any) => {
            let errors = {...prevState.errors};
            if (address === '' ) { errors.addressErr = Constants.required }
            // else if (!mailReg.test(email)) { errors.emailErr = Constants.emailErr }
            else { errors.addressErr = '' }

            if (city === '' ) { errors.cityErr = Constants.required }
            // else if (!passwordReg.test(password)) { errors.passwordErr = Constants.passwordErr }
            else { errors.cityErr = '' }

            if (pincode === '' ) { errors.pincodeErr = Constants.required }
            // else if (!passwordReg.test(password)) { errors.passwordErr = Constants.passwordErr }
            else { errors.pincodeErr = '' }

            if (state === '' ) { errors.stateErr = Constants.required }
            // else if (!passwordReg.test(password)) { errors.passwordErr = Constants.passwordErr }
            else { errors.stateErr = '' }

            if (country === '' ) { errors.countryErr = Constants.required }
            // else if (!passwordReg.test(password)) { errors.passwordErr = Constants.passwordErr }
            else { errors.countryErr = '' }

            return {errors}
        })

        const {addressErr, cityErr, countryErr, pincodeErr, stateErr} = this.state.errors
        if(!addressErr && !cityErr && !countryErr && !pincodeErr && !stateErr){
            const {address, city, pincode, state, country} = this.state;
            // let res = await withTokenPost('address', {address, city, pincode, state, country})
            await withTokenPost('address', {address, city, pincode, state, country})
                .then((res)=>{
                    toastSuccess(`${res.data.message}`)
                    //@ts-ignore
                    this.props.history.push('/profile/Addresses')
                    // <Link to='/' />
                })
                .catch((err)=>{
                    toastError(`${err.response?.data?.message}` || 'Something went wrong');
                })
            // if (res.data.success) {
            //     toastSuccess(`${res.data.message}`)
            // }
            // else {
            //     toastWarning('Something went wrong')
            // }
        }
    }

    render(){
        const {addressErr, cityErr, countryErr, pincodeErr, stateErr} = this.state.errors
        return(
            <div style={{flexGrow: 1}}>
                <Paper elevation={4} style={{width: '100%', justifyContent: 'center',  }}>
                    <Grid container style={{display: 'flex', justifyContent: "center", }} >
                        {/*<Grid item xs={12} style={{marginTop: '10px'}}><h3>Add Address</h3><hr/></Grid>*/}
                        <Grid item container xs={10} spacing={1} style={{paddingTop: 10, marginTop: 20}}>
                            <Paper elevation={4} style={{width: '100%', marginBottom: 20, paddingTop: 20, paddingBottom: 30}}>
                                <TextField
                                    required
                                    //@ts-ignore
                                    error={!!addressErr}
                                    helperText={addressErr}
                                    label="Address"
                                    type="text"
                                    multiline={true}
                                    autoComplete="current-password"
                                    style={customStyle.input}
                                    onChange={event => this.inputChange(event, 'address')}
                                />
                                <TextField
                                    required
                                    //@ts-ignore
                                    error={!!pincodeErr}
                                    helperText={pincodeErr}
                                    label="Pincode"
                                    type="number"
                                    autoComplete="current-password"
                                    style={customStyle.input}
                                    onChange={event => this.inputChange(event, 'pincode')}
                                />
                                <TextField
                                    required
                                    //@ts-ignore
                                    error={!!cityErr}
                                    helperText={cityErr}
                                    label="City"
                                    type="text"
                                    autoComplete="current-password"
                                    style={customStyle.input}
                                    onChange={event => this.inputChange(event, 'city')}
                                />
                                <TextField
                                    required
                                    //@ts-ignore
                                    error={!!stateErr}
                                    helperText={stateErr}
                                    label="State"
                                    type="text"
                                    autoComplete="current-password"
                                    style={customStyle.input}
                                    onChange={event => this.inputChange(event, 'state')}
                                />
                                <TextField
                                    required
                                    //@ts-ignore
                                    error={!!countryErr}
                                    helperText={countryErr}
                                    label="Country"
                                    type="text"
                                    autoComplete="current-password"
                                    style={customStyle.input}
                                    onChange={event => this.inputChange(event, 'country')}
                                />
                                <br/><br/>
                                <div style={{display: 'flex', justifyContent: "space-around"}}>
                                    <Button variant='contained' color='primary' size='large' onClick={this.submit}>Add</Button>
                                    <Button variant='contained' color='primary' size='large'>Cancel</Button>
                                </div>
                            </Paper>
                        </Grid>
                    </Grid>
                </Paper>
            </div>
        );
    }
}


export default AddAddress