import React, {Component} from 'react'
import {Button, Grid, Paper} from "@material-ui/core";
import {withTokenGet} from "../../helper/AxiosGlobal";


class Userdetail extends Component{
    state = {
        Userdetail: {}
    }

    componentDidMount(){
        try{
            withTokenGet('getCustProfile')
            .then(val=>{
                this.setState({Userdetail: val.data.customer_proile})   //what is this
            });
        }
        catch (err){
            console.log(err)
        }

    }

    render() {
        //@ts-ignore
        const {dob, email, gender, first_name, last_name, phone_no} = this.state
        //@ts-ignore
        // console.log('ddddd',dob, email, gender, first_name, last_name, phone_no, "ddddd",this.state.Userdetail.first_name)
        return (
            <div style={{flexGrow: 1}}>
                <Paper elevation={4} style={{width: '100%', justifyContent: 'center',  }}>
                    <Grid container style={{display: 'flex', justifyContent: "center", }} >
                        <Grid item xs={12} style={{marginTop: '10px'}}><h3>Profile</h3><hr/></Grid>
                        <Grid item container xs={12} spacing={1} style={{paddingTop: 10}}>
                            <Grid item xs={12} md={5}><h6 style={{fontWeight: "bold", float: "left", marginLeft: 40}}>First Name:</h6></Grid>
                            {/*
                            //@ts-ignore */}
                            <Grid item xs={12} md={7}><h6 style={{float: "left", marginLeft: 40}}>{this.state.Userdetail.first_name}</h6></Grid>
                            <Grid item xs={12} md={5}><h6 style={{fontWeight: "bold", float: "left", marginLeft: 40}}>Last Name:</h6></Grid>
                            {/*
                            //@ts-ignore */}
                            <Grid item xs={12} md={7}><h6 style={{float: "left", marginLeft: 40}}>{this.state.Userdetail.last_name}</h6></Grid>
                            <Grid item xs={12} md={5}><h6 style={{fontWeight: "bold", float: "left", marginLeft: 40}}>Gender:</h6></Grid>
                            {/*
                            //@ts-ignore */}
                            <Grid item xs={12} md={7}><h6 style={{float: "left", marginLeft: 40}}>{this.state.Userdetail.gender}</h6></Grid>
                            <Grid item xs={12} md={5}><h6 style={{fontWeight: "bold", float: "left", marginLeft: 40}}>Date of Birth:</h6></Grid>
                            {/*
                            //@ts-ignore */}
                            <Grid item xs={12} md={7}><h6 style={{float: "left", marginLeft: 40}}>{this.state.Userdetail.dob}</h6></Grid>
                            <Grid item xs={12} md={5}><h6 style={{fontWeight: "bold", float: "left", marginLeft: 40}}>Mobile Number:</h6></Grid>
                            {/*
                            //@ts-ignore */}
                            <Grid item xs={12} md={7}><h6 style={{float: "left", marginLeft: 40}}>{this.state.Userdetail.phone_no}</h6></Grid>
                            <Grid item xs={12} md={5}><h6 style={{fontWeight: "bold", float: "left", marginLeft: 40}}>Email:</h6></Grid>
                            {/*
                            //@ts-ignore */}
                            <Grid item xs={12} md={7}><h6 style={{float: "left", marginLeft: 40}}>{this.state.Userdetail.email}</h6></Grid>
                            <br/>
                            <br/>
                            <Button variant='outlined' size='large' style={{marginLeft: 40, marginBottom: 40}}>Edit</Button>
                        </Grid>
                    </Grid>
                </Paper>
            </div>
        );
    }
}


export default Userdetail