import React,{Component} from 'react'
import {Button, Grid, IconButton, InputAdornment, Paper, TextField} from "@material-ui/core";
import {RemoveRedEye} from "@material-ui/icons";
import {withTokenDelete, withTokenGet} from "../../helper/AxiosGlobal";
import {toastError, toastSuccess, toastWarning} from "../utils/toast";
import {Link} from "react-router-dom";


//style
const customStyle = {
    mainDiv: {
        display: 'flex',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    div: {
        // height: 750,
        width: 450,
        marginTop: 20,
        paddingTop: 10,
        paddingBottom: 20,
        marginBottom: 20,
        borderColor: 'black',
        textAlign: 'center'
    },
    input: {width: '70%'}
};

class Addresses extends Component{
    state = {
        addresses: []
    }

    componentDidMount(){
        try{
            withTokenGet('getCustAddress')
            .then(val =>{ this.setState({addresses: val.data.customer_address}) })
            .catch(err =>{ toastError(`${err.response?.data?.message}`) })
        }
        catch (err){
            toastError(`${err.response?.data?.message}` || 'Something went wrong');
        }
    }

    show = () => {
        const {addresses} = this.state;
        return addresses.map(val => {
            return(
                <Paper elevation={4} style={{width: '100%', marginBottom: 20, paddingTop: 20}}>
                    {/*
                    //@ts-ignore */}
                    <p>{val.address}, {val.city}, {val.pincode}, {val.state}, {val.country}</p>
                    <Button variant='contained' color='primary' style={{marginBottom: 20, float: "left", marginLeft: 25}}>Edit</Button>
                    {/*
                    //@ts-ignore */}
                    <Button onClick={()=>{this.addressDelete(val.address_id)}} variant='contained' color='secondary' style={{marginBottom: 20, float: "left", marginLeft: 10}}>Delete</Button>
                </Paper>
            )
        })
    }

    addressDelete = (id: number) => {
        try{
            withTokenDelete(`deladdress/${id}`)
            .then((deladdval)=>{
                toastSuccess(deladdval.data.message || 'Successfully deleted address')
                try{
                    withTokenGet('getCustAddress')
                    .then(val =>{ this.setState({addresses: val.data.customer_address}) })
                    .catch(err =>{
                        toastError(`${err.response?.data?.message}` || 'Something went wrong');
                        this.setState({addresses: []})
                    })
                }
                catch(err) { toastError(`${err.response?.data?.message}` || 'Something went wrong'); }
            })
            .catch(err =>{ toastError(`${err.response?.data?.message}`) })
        }
        catch(err) { toastError(`${err.response?.data?.message}` || 'Something went wrong'); }
    }

    render(){
        const {} = this.state
        return(
            <div style={{flexGrow: 1}}>
                <Paper elevation={4} style={{width: '100%', justifyContent: 'center',  }}>
                    <Grid container style={{display: 'flex', justifyContent: "center", }} >
                        <Grid item xs={12} style={{marginTop: '10px'}}><h3>Addresses</h3><hr/></Grid>
                        <Grid item container xs={10} spacing={1} style={{paddingTop: 10, }}>
                            {this.show()}
                            <Paper elevation={4} style={{width: '100%', marginBottom: 20, paddingTop: 20}}>
                                {/*
                                //@ts-ignore */}
                                {/*<Button variant='contained' color='primary' style={{marginBottom: 20, float: "left", marginLeft: 25}} onClick={()=>{this.props.history.push('/profile/AddAddress')}}>Add</Button>*/}
                                <Button variant='contained' color='primary' style={{marginBottom: 20, float: "left", marginLeft: 25}} ><Link to={'/profile/AddAddress'} style={{textDecoration: 'none', color: 'white'}}>Add</Link></Button>
                                <div style={{float: 'left', marginLeft: 20}}>Add new Address.</div>
                            </Paper>
                        </Grid>
                    </Grid>
                </Paper>
            </div>
        );
    }
}

export default Addresses