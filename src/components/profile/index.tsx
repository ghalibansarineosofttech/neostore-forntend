import React, {Component} from 'react'
import {Button, Container, Grid, Paper} from '@material-ui/core';
import {Link, Redirect, Route, Switch} from "react-router-dom";
import Home from "../home";
import Userdetail from './userdetail'
import ChangePassword from './ChangePassword'
import Addresses from "./Addresses";
import Order from "./Order";
import AddAddress from "./AddAddress";


class profile extends Component {
    state = {
        currentPage: 'Userdetail',
        miniPage: <Userdetail />
    }

    componentDidMount(): void {
        const token: any = localStorage.getItem('token');
        if (token === null) {
            //@ts-ignore
            this.props.history.push('/login')
        }
        // else {
        //     @ts-ignore
            // this.props.history.push('/login')
        // }
        if(this.props){
            //@ts-ignore
            const {link} = this.props.match.params
            //@ts-ignore
            this.setState((prevState: any)=>{
                if(link==='Userdetail'){
                    prevState.currentPage = 'Userdetail'
                    prevState.miniPage = <Userdetail />
                } else if(link==='ChangePassword'){
                    prevState.currentPage = 'ChangePassword'
                    prevState.miniPage = <ChangePassword />
                } else if(link==='Addresses'){
                    prevState.currentPage = 'Addresses'
                    prevState.miniPage = <Addresses />
                } else if(link==='Order'){
                    prevState.currentPage = 'Order'
                    prevState.miniPage = <Order />
                } else if(link==='AddAddress'){
                    prevState.currentPage = 'AddAddress'
                    prevState.miniPage = <AddAddress />
                }
                return prevState
            })
            // this.setState({currentPage: this.props?.match?.params?.link})
        }
    }

    show = (val: any) => {
        const {currentPage, miniPage} = this.state
        this.setState((prevState: any)=>{
            if(val==='Userdetail'){
                prevState.currentPage = 'Userdetail'
                prevState.miniPage = <Userdetail />
            } else if(val==='ChangePassword'){
                prevState.currentPage = 'ChangePassword'
                prevState.miniPage = <ChangePassword />
            } else if(val==='Addresses'){
                prevState.currentPage = 'Addresses'
                prevState.miniPage = <Addresses />
            } else if(val==='Order'){
                prevState.currentPage = 'Order'
                prevState.miniPage = <Order />
            } else if(val==='AddAddress'){
                prevState.currentPage = 'AddAddress'
                prevState.miniPage = <AddAddress />
            }
            return prevState
        })
    }

    render() {
        const {miniPage} = this.state
        return(
            <div style={{flexGrow: 1}}>
            <Grid container style={{display: 'flex', justifyContent: "center", paddingTop: '40px'}} >
                <Paper elevation={4} style={{width: '100%', justifyContent: 'center',  paddingBottom: '40px'}}>
                <Grid container>
                    <Grid item xs={11} style={{width: '100%', margin: 'auto', paddingTop: '30px'}}>
                        <Paper elevation={4}>
                            <h2>My Account</h2>
                        </Paper>
                    </Grid>
                    <Grid container xs={11} item spacing={2} style={{display: 'flex', paddingTop: '30px', margin: 'auto'}}>
                        <Grid item xs={12} md={3} >
                            <Paper elevation={4} style={{textAlign: 'center', width: '100%'}}>
                                <Grid container>
                                    <Grid item xs={11} style={{margin: '20px'}}>
                                        <Paper elevation={3} style={{textAlign: 'center', width: '100%', background: 'yellow', margin: 'auto'}}>
                                        <img style={{width: '100%'}} src='https://media.istockphoto.com/vectors/entrepreneur-icon-vector-person-profile-avatar-symbol-with-bulb-for-vector-id956565836' />
                                        <h4>First Lady</h4>
                                        </Paper>
                                    </Grid>
                                    <Grid xs={12} style={{paddingTop: '20px', paddingBottom: '20px'}}>
                                        <Button variant="outlined" style={{width: '80%'}} onClick={()=>{this.show('Order')}}><Link to='/profile/Order' style={{textDecoration: "none", color: "black"}}>Order</Link></Button>
                                        <Button variant="outlined"  style={{width: '80%'}} onClick={()=>{this.show('Userdetail')}}><Link to='/profile/Userdetail' style={{textDecoration: "none", color: "black"}}>Profile</Link></Button>
                                        <Button variant="outlined" style={{width: '80%'}} onClick={()=>{this.show('Addresses')}}><Link to='/profile/Addresses' style={{textDecoration: "none", color: "black"}}>Address</Link></Button>
                                        <Button variant="outlined" style={{width: '80%'}} onClick={()=>{this.show('ChangePassword')}}><Link to='/profile/ChangePassword' style={{textDecoration: "none", color: "black"}}>Change Password</Link></Button>
                                    </Grid>
                                </Grid>
                            </Paper>
                        </Grid>
                        <Grid item xs={12} md={9} >
                            <Paper style={{width: '100%', background: 'pink'}}>
                                {miniPage}
                                {/*<Userdetail />*/}
                                {/*<ChangePassword />*/}
                                {/*<Addresses />*/}
                                {/*<Order />*/}
                            </Paper>
                        </Grid>
                    </Grid>
                </Grid>
                </Paper>
            </Grid>
            </div>
        )
    }
}


export default profile