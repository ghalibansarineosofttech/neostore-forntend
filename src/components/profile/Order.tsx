import React,{Component} from 'react'
import {Button, Grid, IconButton, InputAdornment, Paper, TextField} from "@material-ui/core";
import {RemoveRedEye} from "@material-ui/icons";
import {withTokenGet, withTokenPost} from "../../helper/AxiosGlobal";
import {Constants} from "../../helper/constant";



//style
const customStyle = {
    mainDiv: {
        display: 'flex',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    div: {
        // height: 750,
        width: 450,
        marginTop: 20,
        paddingTop: 10,
        paddingBottom: 20,
        marginBottom: 20,
        borderColor: 'black',
        textAlign: 'center'
    },
    input: {width: '70%'}
};

class Order extends Component{
    state = {
        order: {}
    }

    componentDidMount(){
        try{
            withTokenGet('getOrderDetails')
            .then(val=>{
                this.setState({order: val.data})
            });
        }
        catch (err){
            console.log("erroror",err)
        }
    }

    show = () => {
        const {order} = this.state;
        //@ts-ignore
        return order?.product_details?.map(val => {
            console.log("ggghhh",val)
            return(
                <Paper key={val._id} elevation={4} style={{width: '100%', margin: 20, }}>
                    <Grid container style={{padding: 10}}>
                        <Grid item xs={12} md={3}><img style={{width: '100%', height: '100%'}} src={`${Constants.baseURL}${val.product_details[0].product_details[0].product_image}`} /></Grid>
                        {/*<Grid item xs={12} md={3}><img style={{width: '100%'}} src='https://media.istockphoto.com/vectors/entrepreneur-icon-vector-person-profile-avatar-symbol-with-bulb-for-vector-id956565836' /></Grid>*/}
                        <Grid item xs={12} md={6} style={{margin: 'auto'}}><h4>Order: {val._id}</h4><p>Placed on: 26/03/2020 / Rs.{val.product_details[0].total_cartCost}</p></Grid>
                        <Grid item xs={12} md={3} style={{margin: 'auto'}}><Button variant='contained' color='primary' onClick={()=>{this.pdf(val)}}>Download invoice as PDF</Button></Grid>
                    </Grid>
                </Paper>
            )
        })
    }

    pdf = async (object: any) => {
        await withTokenPost('getInvoiceOfOrder', object)
        .then(res=>{
            console.log(Constants.baseURL,res.data.receipt,"receipt")
            //@ts-ignore
            // this.props.history.push({pathname: `${Constants.baseURL}${res.data.receipt}`, target: '_blank'});
            //@ts-ignore
            window.open(`${Constants.baseURL}${res.data.receipt}`, "_blank");
        })
    }

    render(){
        // const {oldPasswordIsMasked} = this.state;
        return(
            <div style={{flexGrow: 1}}>
                <Paper elevation={4} style={{width: '100%', justifyContent: 'center',  }}>
                    <Grid container style={{display: 'flex', justifyContent: "center", }} >
                        <Grid item xs={12} style={{marginTop: '10px'}}><h3>Orders.</h3><hr/></Grid>
                        <Grid item container xs={12} spacing={1} style={{paddingTop: 10, }}>
                            {this.show()}
                            {/*<Paper elevation={4} style={{width: '100%', margin: 20, }}>*/}
                            {/*    <Grid container style={{padding: 10}}>*/}
                            {/*        <Grid item xs={12} md={3}><img style={{width: '100%'}} src='https://media.istockphoto.com/vectors/entrepreneur-icon-vector-person-profile-avatar-symbol-with-bulb-for-vector-id956565836' /></Grid>*/}
                            {/*        <Grid item xs={12} md={6} style={{margin: 'auto'}}><h4>Order: ORDERNO_394</h4><p>Placed on: 26/03/2020 / ₹42000</p></Grid>*/}
                            {/*        <Grid item xs={12} md={3} style={{margin: 'auto'}}><Button variant='contained' color='primary' >Download invoice as PDF</Button></Grid>*/}
                            {/*    </Grid>*/}
                            {/*</Paper>*/}
                        </Grid>
                    </Grid>
                </Paper>
            </div>
        );
    }
}

export default Order;