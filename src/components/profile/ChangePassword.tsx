import React,{Component} from 'react'
import {Button, Grid, IconButton, InputAdornment, Paper, TextField} from "@material-ui/core";
import {RemoveRedEye} from "@material-ui/icons";
import {Constants} from "../../helper/constant";
import {withOutTokenPost} from "../../helper/AxiosGlobal";
import {toastError, toastSuccess} from "../utils/toast";
import {Loader} from "../utils/loader";
import VisibilityOffIcon from "@material-ui/icons/VisibilityOff";
import VisibilityIcon from "@material-ui/icons/Visibility";


//style
const customStyle = {
    mainDiv: {
        display: 'flex',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    div: {
        // height: 750,
        width: 450,
        marginTop: 20,
        paddingTop: 10,
        paddingBottom: 20,
        marginBottom: 20,
        borderColor: 'black',
        textAlign: 'center'
    },
    input: {width: '70%'}
};

class ChangePassword extends Component{
    state = {
        oldPasswordIsMasked: true,
        newPasswordIsMasked: true,
        cfmPasswordIsMasked: true,
        errors: {
            oldPassErr: '',
            newPassErr: '',
            confirmPassErr: '',
        },
        oldPass: '',
        newPass: '',
        confirmPass: '',
        loading: false,
    }

    inputChange = async (event: any, inputType: string) => {
        await this.setState({[inputType]: event.target.value});
        const {oldPass, newPass, confirmPass} = this.state;
        const passReg: any = Constants.passwordReg
        await this.setState((prevState: any) => {
            let errors = {...prevState.errors};
            if (inputType === 'oldPass') {
                if (oldPass === '' ) { errors.oldPassErr = Constants.required }
                else if (!passReg.test(oldPass)) { errors.oldPassErr = Constants.passwordErr }
                else { errors.oldPassErr = Constants.blank }
            }
            if (inputType === 'newPass') {
                if (newPass === '' ) { errors.newPassErr = Constants.required }
                else if (!passReg.test(newPass)) { errors.newPassErr = Constants.passwordErr }
                else if (newPass !== confirmPass && confirmPass !== '') { errors.newPassErr = 'Password does not match' }
                else { errors.newPassErr = Constants.blank }
            }
            if (inputType === 'confirmPass') {
                if (confirmPass === '' ) { errors.confirmPassErr = Constants.required }
                else if (!passReg.test(confirmPass)) { errors.confirmPassErr = Constants.passwordErr }
                else if (newPass !== confirmPass && newPass !== '') { errors.confirmPassErr = 'Password does not match' }
                else { errors.confirmPassErr = Constants.blank }
            }
            return {errors}
        })
    }

    submit = async () => {
        this.setState({loading: true});
        // await this.setState({[inputType]: event.target.value});
        const {oldPass, newPass, confirmPass} = this.state;
        const passReg: any = Constants.passwordReg
        await this.setState((prevState: any) => {
            let errors = {...prevState.errors};

            if (oldPass === '' ) { errors.oldPassErr = Constants.required }
            else if (!passReg.test(oldPass)) { errors.oldPassErr = Constants.passwordErr }
            else { errors.oldPassErr = Constants.blank }

            if (newPass === '' ) { errors.newPassErr = Constants.required }
            else if (!passReg.test(newPass)) { errors.newPassErr = Constants.passwordErr }
            else if (newPass !== confirmPass && confirmPass !== '') { errors.newPassErr = 'Password does not match' }
            else { errors.newPassErr = Constants.blank }

            if (confirmPass === '' ) { errors.confirmPassErr = Constants.required }
            else if (!passReg.test(confirmPass)) { errors.confirmPassErr = Constants.passwordErr }
            else if (newPass !== confirmPass && newPass !== '') { errors.confirmPassErr = 'Password does not match' }
            else { errors.confirmPassErr = Constants.blank }

            return {errors}
        })

        if (passReg.test(oldPass) && passReg.test(newPass) && passReg.test(confirmPass)) {
            const data = {oldPass, newPass, confirmPass}
            try {
                let res = await withOutTokenPost('changePassword', data);
                if (res.data.success) {
                    toastSuccess(res.data.message);
                    this.setState({loading: false});
                    //@ts-ignore
                    // this.props.history.push('/login')
                } else {
                    this.setState({loading: false});
                    toastError("Something went wrong.")
                }
            } catch (err) {
                console.log(err.response.data.message,"err")
                this.setState({loading: false});
                toastError(`${err.response.data.message}. please try again.`)
            }
        }
        this.setState({loading: false});
    }

    toggleOldPasswordMask = () => { this.setState({ oldPasswordIsMasked: !this.state.oldPasswordIsMasked }) };
    toggleNewPasswordMask = () => { this.setState({ newPasswordIsMasked: !this.state.newPasswordIsMasked }) };
    toggleCfmPasswordMask = () => { this.setState({ cfmPasswordIsMasked: !this.state.cfmPasswordIsMasked }) };

    render(){
        const {oldPasswordIsMasked, newPasswordIsMasked, cfmPasswordIsMasked} = this.state
        const {oldPassErr, newPassErr, confirmPassErr} = this.state.errors;
        return(
            <div style={{flexGrow: 1}}>
                {/*
                //@ts-ignore */}
                <Loader loading={this.state.loading}/>
                <Paper elevation={4} style={{width: '100%', justifyContent: 'center',  }}>
                    <Grid container style={{display: 'flex', justifyContent: "center", }} >
                        <Grid item xs={12} style={{marginTop: '10px'}}><h3>Change Password</h3><hr/></Grid>
                        <Grid item container xs={10} spacing={1} style={{paddingTop: 10, justifyContent: "center", }}>
                            <Paper elevation={4} style={{width: '100%', marginBottom: 40, marginTop: 10, paddingTop: 20}}>
                            <TextField
                                required
                                id=""
                                helperText={oldPassErr}
                                error={!!oldPassErr}
                                label="Old Password"
                                type={oldPasswordIsMasked ? 'password' : 'text'}
                                // value={programName}
                                style={customStyle.input}
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            <IconButton
                                                aria-label="toggle password visibility"
                                                onClick={this.toggleOldPasswordMask}
                                            >
                                                {/*<RemoveRedEye/>*/}
                                                {oldPasswordIsMasked ? <VisibilityOffIcon/> : <VisibilityIcon/>}
                                            </IconButton>
                                        </InputAdornment>
                                    ),
                                }}
                                onChange={event => this.inputChange(event, 'oldPass')}
                            />
                            <br/><br/>
                            <TextField
                                required
                                id=""
                                helperText={newPassErr}
                                error={!!newPassErr}
                                label="New Password"
                                type={newPasswordIsMasked ? 'password' : 'text'}
                                // value={programName}
                                style={customStyle.input}
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            <IconButton
                                                aria-label="toggle password visibility"
                                                onClick={this.toggleNewPasswordMask}
                                            >
                                                {/*<RemoveRedEye/>*/}
                                                {newPasswordIsMasked ? <VisibilityOffIcon/> : <VisibilityIcon/>}
                                            </IconButton>
                                        </InputAdornment>
                                    ),
                                }}
                                onChange={event => this.inputChange(event, 'newPass')}
                            />
                            <br/><br/>
                            <TextField
                                required
                                id=""
                                helperText={confirmPassErr}
                                error={!!confirmPassErr}
                                label="Confirm Password"
                                type={cfmPasswordIsMasked ? 'password' : 'text'}
                                // value={programName}
                                style={customStyle.input}
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            <IconButton
                                                aria-label="toggle password visibility"
                                                onClick={this.toggleCfmPasswordMask}
                                            >
                                                {/*<RemoveRedEye/>*/}
                                                {cfmPasswordIsMasked ? <VisibilityOffIcon/> : <VisibilityIcon/>}
                                            </IconButton>
                                        </InputAdornment>
                                    ),
                                }}
                                onChange={event => this.inputChange(event, 'confirmPass')}
                            />
                            <br/><br/>
                            <Button variant='outlined' onClick={this.submit} size='large' style={{marginBottom: 30}}>Submit</Button>
                            </Paper>
                        </Grid>
                    </Grid>
                </Paper>
            </div>
        );
    }
}


export default ChangePassword