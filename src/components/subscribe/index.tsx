import React, { Component } from 'react'
import Grid from '@material-ui/core/Grid';


export default class Subscribe extends Component {

    render() {
        return (
           <Grid container xs={12} style={{display: 'flex', justifyContent: 'center', paddingTop: '100px'}}>
               {/*
               //@ts-ignore */}
               <h1>Thank You {this.props.match.params.email} For Subscribing</h1>
           </Grid>
        )
    }
}
