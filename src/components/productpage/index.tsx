import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import Grid from '@material-ui/core/Grid';
import {withOutTokenGet} from '../../helper/AxiosGlobal'
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Pagination from '@material-ui/lab/Pagination';
import Typography from '@material-ui/core/Typography';
import {Constants} from '../../helper/constant';
import Rating from '@material-ui/lab/Rating';
import Box from '@material-ui/core/Box';
import Tooltip from '@material-ui/core/Tooltip';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {Paper} from '@material-ui/core'
import StarIcon from '@material-ui/icons/Star';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';

export default class Productpage extends Component {
    state = {
        cards: [],
        getAllCategories: [],
        getAllColors: [],
        pageName: 'All Products',
        expanded: '',
        total_count: 0,
        filters: {
            category_id: '',
            color_id: '',
            sortBy: '',
            sortIn: '',
            pageNo: 1,
            perPage: 9
        }
    };

    componentDidMount() {
        //@ts-ignore
        if (this.props.match.params.category) {
            //@ts-ignore
            this.commonproducts(this.props.match.params.category)
        } else {
            this.commonproducts()
        }  //Todo solve category pagename bug...

        withOutTokenGet('getAllCategories')
            .then((data) => {
                this.setState({getAllCategories: data.data.category_details})
            });

        withOutTokenGet('getAllColors')
            .then((data) => {
                this.setState({getAllColors: data.data.color_details})
            })
    }

    nocard = () => {
        return (<h1>No Products Available.</h1>)
    };

    card = () => {
        let {cards} = this.state;
        return cards.map((value: any) => {
            return (
                <Grid item key={value._id} xs={12} sm={6} md={4} xl={3}>
                    <Box boxShadow={3}>
                        <Card>
                            {/*
                    //@ts-ignore */}
                            <CardActionArea onClick={() => {
                                {/*
                                //@ts-ignore */}
                                this.props.history.push(`/productdetailpage/${value?.product_id}`)
                            }}>
                                <Link to={`/productdetailpage/${value?.product_id}`} />
                                <CardMedia
                                    component="img"
                                    style={{height: '200px'}}
                                    image={`${Constants.baseURL}${value.product_image}`}
                                    title={value.product_name}
                                />
                                <CardContent>
                                    <Typography gutterBottom>{value.product_name}</Typography>
                                </CardContent>
                            </CardActionArea>
                            <CardActions>
                                <Grid container item style={{display: 'flex', justifyContent: 'center'}}>
                                    <Grid xs={12} sm={4} md={4} xl={4}>Rs.{value.product_cost}</Grid>
                                    <Grid xs={12} sm={8} md={8} xl={8}><Rating name="read-only"
                                                                               value={value.product_rating}
                                                                               readOnly/></Grid>
                                    <Grid xs={10} style={{marginTop: '10px'}}><Button variant="contained"
                                                                                      color="primary">Add to
                                        Cart</Button></Grid><br/>
                                </Grid>
                            </CardActions>
                        </Card>
                    </Box>
                </Grid>
            )
        })
    };

    commonproducts = (category_id: string = this.state.filters.category_id, color_id: string = this.state.filters.color_id, pageNo: number = this.state.filters.pageNo, perPage: number = this.state.filters.perPage, sortBy: string = this.state.filters.sortBy, sortIn: string = this.state.filters.sortIn) => {
        withOutTokenGet(`commonProducts?category_id=${category_id}&color_id=${color_id}&sortBy=${sortBy}&sortIn=${sortIn}&name=${''}&pageNo=${pageNo}&perPage=${perPage}`)
            .then((data) => {
                if (data.data.success) {
                    this.setState({
                        cards: data.data.product_details,
                        total_count: data.data.total_count,
                        filters: {category_id, color_id, pageNo, perPage, sortBy, sortIn}
                    })
                } else {
                    this.setState({cards: []})
                }
            })
    };

    getAllCategoriesfunction = () => {
        const {color_id} = this.state.filters;
        return this.state.getAllCategories.map((val: any) => {
            return <Button key={val._id} size="large" variant="outlined" onClick={() => {
                this.commonproducts(`${val._id}`, color_id, 1);
                this.setState({pageName: val.category_name})
            }}>{val.category_name}</Button>
        })
    };

    getAllColorfunction = () => {
        return this.state.getAllColors.map((val: any) => {
            return (<Grid item sm={4} md={6}>
                <Tooltip title={`${val.color_name}`} arrow style={{fontSize: "2em"}}>
                    <Button key={val.color_id} size="small" variant="contained"
                            style={{backgroundColor: `${val.color_code}`, height: '35px'}} onClick={() => {
                        this.commonproducts(this.state.filters.category_id, `${val._id}`, 1)
                    }}/>
                </Tooltip>
            </Grid>)
        })
    };

    pagination = () => {
        const {total_count} = this.state;
        const {pageNo} = this.state.filters;
        let pages = Math.ceil(total_count / 9);
        return (
            <Pagination count={pages} showFirstButton showLastButton page={pageNo}
                        onChange={(event: any, page: any) => this.commonproducts(this.state.filters.category_id, this.state.filters.color_id, page)}
                        color="primary" size={"large"} style={{paddingTop: '20px', paddingBottom: '20px'}}/>
        )
    };


    render() {
        //@ts-ignore
        const {cards, total_count} = this.state;
        const {category_id, color_id, pageNo, perPage, sortBy, sortIn} = this.state.filters;
        return (
            <Grid container style={{display: 'flex', justifyContent: 'center', paddingTop: '50px'}} spacing={4}>
                <Grid container item style={{display: 'flex', flexDirection: 'column', paddingBottom: '10px'}} xs={12}
                      sm={12} md={3} xl={3} >
                    <Grid item xs={12}>
                        <Card style={{
                            display: 'flex',
                            justifyContent: 'center',
                            paddingBottom: '20px',
                            paddingTop: '20px'
                        }}>
                            <Grid item xs={10}>
                                <Paper><Button size="large" onClick={() => {
                                    this.commonproducts('', '', 1, 9)
                                }}>All Products</Button>
                                </Paper>
                                <h1>Filters</h1>
                                {/*
                        //@ts-ignore */}
                                {/* <ExpansionPanel expanded={this.state.expanded === 'panel1'} onChange={this.handleChange('panel1')}> */}
                                <ExpansionPanel>
                                    <ExpansionPanelSummary
                                        expandIcon={<ExpandMoreIcon/>}
                                        aria-controls="panel1a-content"
                                        id="panel1a-header"
                                    >
                                        <Typography variant="h6">Categories</Typography>
                                    </ExpansionPanelSummary>
                                    <ExpansionPanelDetails style={{display: 'flex', flexDirection: 'column'}}>
                                        {this.state.getAllCategories.length === 0 ?
                                            <h4>No category available</h4> : this.getAllCategoriesfunction()}
                                    </ExpansionPanelDetails>
                                </ExpansionPanel>
                                {/*
                        //@ts-ignore */}
                                <ExpansionPanel>
                                    <ExpansionPanelSummary
                                        expandIcon={<ExpandMoreIcon/>}
                                        aria-controls="panel1a-content"
                                        id="panel1a-header"
                                    >
                                        <Typography variant="h6">Color</Typography>
                                    </ExpansionPanelSummary>
                                    <ExpansionPanelDetails style={{display: 'flex', justifyContent: 'center'}}>
                                        <Grid container item sm={12} spacing={3}>
                                            {this.state.getAllColors.length === 0 ?
                                                <h4>No color available</h4> : this.getAllColorfunction()}
                                        </Grid>
                                    </ExpansionPanelDetails>
                                </ExpansionPanel>
                            </Grid>
                        </Card>
                    </Grid>
                </Grid>

                <Grid container item xs={12} sm={12} md={9} xl={9} spacing={3}>
                    <Grid container item sm={12} style={{display: 'flex', height: '70px'}}>
                        <Grid item xs={12} md={6} style={{display: 'flex', justifyContent: 'flex-start'}}>
                            <h1>{this.state.pageName}</h1></Grid>
                        <Grid item xs={12} md={6} style={{display: 'flex', justifyContent: 'flex-end'}}>
                            <Grid style={{display: 'flex', flex: 'column'}}>
                                <h1>Sort By: </h1>
                                <Button><StarIcon style={{color: "green"}} fontSize="large"  onClick={() => {
                                    this.commonproducts('', '', 1, perPage, 'product_rating', 'true')
                                }}/></Button>
                                <Button>₹<ArrowUpwardIcon style={{color: "green"}} fontSize="large" onClick={() => {
                                    this.commonproducts('', '', 1, perPage, 'product_cost', 'true')
                                }}/></Button>
                                <Button>₹<ArrowDownwardIcon style={{color: "green"}} fontSize="large" onClick={() => {
                                    this.commonproducts('', '', 1, perPage, 'product_cost', 'false')
                                }}/></Button>
                            </Grid>
                        </Grid>
                    </Grid>
                    {/*<Grid xs={12}>*/}
                    {/*
                    //@ts-ignore */}
                    {cards.length === 0 ? this.nocard() : this.card()}
                    {/*</Grid>*/}
                    <Grid xs={12}>
                        {total_count > 9 ? this.pagination() : ""}
                    </Grid>
                </Grid>
            </Grid>
        )
    }
}

