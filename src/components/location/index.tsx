import React, { Component } from 'react'
//@ts-ignore
import GoogleMapReact from 'google-map-react';

//@ts-ignore
const AnyReactComponent = ({ text }) => <div>{text}</div>;


export default class Location extends Component {
    state = {
        center: {
            lat: 59.95,
            lng: 30.33
        },
        zoom: 11
    };

    render() {
        return (
            <div style={{ height: '500px', width: '100%', paddingTop: '50px', paddingBottom: '50px'}}>
                <GoogleMapReact
                    bootstrapURLKeys={{ key: 'YOUR KEY HERE' }}
                    defaultCenter={this.state.center}
                    defaultZoom={this.state.zoom}
                >
                <AnyReactComponent
                    // lat={59.955413}
                    // lng={30.337844}
                    text="My Marker"
                />
                </GoogleMapReact>
            </div>
        )
    }
}
